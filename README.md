# Pylint

Gitlab CI for pylint

- runs over git push when files with .py extension change for any branch/tag
- allowed to fail

## How to use :

Include in the .gitlab-ci.yml

```yaml

pylint:
  stage: lint
  extends: .pylint
  variables:
    PYLINT_PY_VER: "2"

include:
  - project: "amitdalal/pylint"
    file: "/ci.yml"

```

Set value of PYLINT_PY_VER as either 2, 3, 3.8.5, 3.7 or 3.6.11 or whatever to specify python version to use
